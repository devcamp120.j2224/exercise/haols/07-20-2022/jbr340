package com.devcamp.s10.jbr340.restapi;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CircleController {
    @CrossOrigin
    @GetMapping("/circle-area")
    public double getCircleArea(@RequestParam(required = true) double paramRadius) {
        Circle circle = new Circle(paramRadius);
        System.out.println(circle);
        return circle.getArea();
    }
}
